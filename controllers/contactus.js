dataBase = require('../models');
const contactus = dataBase.contactus;
const sequelize = require("sequelize");
exports.createContactUs = async (req, res, next) => {

    contactus.create({
        name:req.body.name,
        email:req.body.email,
        message:req.body.message,
       
    }).then(resData=>{                
        return res.status(200).json({
            message:'Created successfully'
        })
    }).catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.getAllContactUs = async (req, res, next) => {

    contactus.findAll({
        order: [["updatedAt" ,'DESC']],
    })
    .then(resData=>{                
        return res.status(200).json({
            contactUs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}