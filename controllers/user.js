const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var otpGenerator = require('otp-generator');
dataBase = require('../models');
const User = dataBase.User;
const sequelize = require("sequelize");
const booking = dataBase.Booking;

exports.createUser = async (req, res, next) => {
      console.log("\n\nCreate user starts here\n\n");

      //basic requirements check 

      if (!req.body.user) {
        return res.status(400).json({
          message: "user object is not present!"
        });
      }

      if (!req.body.user.password) {
        return res.status(400).json({
          message: "password is not present!"
        });
      }

      if (!req.body.user.email) {
        return res.status(400).json({
          message: "email is not present!"
        });
      }

      if (!req.body.user.firstName) {
        return res.status(400).json({
          message: "first name is not present!"
        });
      } 
      if (!req.body.user.lastName) {
        return res.status(400).json({
          message: "Last Name is not present!"
        });
      }
      if (!req.body.user.user_role) {
        return res.status(400).json({
          message: "user_role is not present!"
        });
      }
          //creating company object
              //creating user object
              bcrypt.hash(req.body.user.password, 10).then(hash => {
                let userObject={};
                if(req.body.user.user_role=='admin')
                {
                  userObject = {
                    firstName: req.body.user.firstName,
                    lastName: req.body.user.lastName,
                    email: req.body.user.email,
                    encryptedPassword: hash,
                    lastSignIn: null,
                    IP: req.headers['x-forwarded-for'] ||
                      req.socket.remoteAddress ||
                      '',
                    signInCount: null,
                    mobile: req.body.user.mobile || null,
                    user_role: req.body.user.user_role,
                  //  companyId: companyID,
                    verifed: true,
                    blackList: false                 
                  };  
                }
                else{
                  userObject = {
                    firstName: req.body.user.firstName,
                    lastName: req.body.user.lastName,
                    email: req.body.user.email,
                    encryptedPassword: hash,
                    lastSignIn: null,
                    IP: req.headers['x-forwarded-for'] ||
                      req.socket.remoteAddress ||
                      '',
                    signInCount: null,
                    mobile: req.body.user.mobile || null,
                    user_role: req.body.user.user_role,
                  //  companyId: companyID,
                    verifed: false,
                    blackList: false                 
                  };  
                }
                User.create(userObject)
                  .then(userResponse => {
                  

                    return res.status(200).json({
                      message: "User Created  Successful.",
                      user: userResponse,

                    })


            }) // to observe response async
            .catch(err => {
              return res.status(400).json({
                message: "Company Creation failed!",
                error: err.message || "Internal server Error"
              })
            }); 
  })};

exports.userLogin = (req, res, next) => {
  let fetchedUser;

  if (req.body.email) {
    User.findOne({ where: { email: req.body.email } }
    )
      .then(user => {
        if (!user) {
          return res.status(401).json({
            message: "Auth failed,email not found"
          });
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.encryptedPassword);
      })
      .then(result => {
        if (!result) {
          return res.status(401).json({
            message: "Auth failed,password mismatched."
          });
        }
        if (fetchedUser.verifed)
        {
        const token = jwt.sign(
          { email: fetchedUser.email, userId: fetchedUser.id },
          "akmakndalknadfa"
        );
           res.status(200).json({
              token: token,
              user: fetchedUser
            });
        }else{
          return res.status(400).json({
            message: "User not verified."
          });
        }


      })
      .catch(err => {
        return res.status(401).json({
          message: "Invalid authentication credentials!"
        });
      });
  }
  else {
    return res.status(400).json({
      message: "email and mobile missing!"
    });
  }
  
};


exports.getUserById = (req, res, next) => {

      if (!req.query.id) {
        return res.status(400).json({
          message: "User Id not present"
        });
      }
      User.findByPk(req.query.id)
        .then(result => {

    
      const user = {
        id: result.id,
        firstName: result.firstName || '',
        lastName: result.lastName || '',
        email: result.email || '',
        mobile: result.mobile,
        user_role: result.user_role || '',
      
      }
      
          return res.status(200).json({
                message: "Success!",
                user: user, 
                  })})
               .catch(
                err => {
                  return res.status(500).json({
                    message: "User Privilege Id present but  data fetching failed",
                    error: err.message || "internal server error!"
                  });

                }
              )
        
};

exports.updateUserById = async (req, res, next) => {
      if (!req.body.id) {
        return res.status(400).json({
          message: "User Id not present!"
        })
      }
      if (!req.body) {
        return res.status(400).json({
          message: "user object is not present!"
        });
      }


        User.findByPk(req.body.id)
         .then(userResult => {
     

            userResult.firstName = req.body.firstName || userResult.firstName;
            userResult.lastName = req.body.lastName || userResult.lastName;
            userResult.email = req.body.email || userResult.email;
            userResult.mobile = req.body.mobile || userResult.mobile;
            userResult.user_role = req.body.user_role || userResult.user_role;
            
            userResult.save()
              .then(result => {

                res.status(200).json({
                  message: "Operation Sucessful",
                  user: result
                

                 })    }) 
              
              .catch(err => {
                return res.status(500).json({
                  message: "User Update failed",
                  error: err.message || "internal server error!"
                });
              });
            });

}




exports.verifyuser = async (req, res, next) => {
   
  User.findByPk(req.query.id)
  .then(userresult => {


    userresult.verifed = true;

     
        userresult.save()
       .then(result => {

         res.status(200).json({
           message: "Operation Sucessful",
           user: result
         

          })    }) 
       
       .catch(err => {
         return res.status(500).json({
           message: "User Update failed",
           error: err.message || "internal server error!"
         });
       });
     });


}

exports.unverifyuser = async (req, res, next) => {
   
  User.findByPk(req.query.id)
  .then(userresult => {


    userresult.verifed = false;

     
        userresult.save()
       .then(result => {

         res.status(200).json({
           message: "Operation Sucessful",
           user: result
         

          })    }) 
       
       .catch(err => {
         return res.status(500).json({
           message: "User Update failed",
           error: err.message || "internal server error!"
         });
       });
     });


}

exports.getAllUsers = async (req, res, next) => {
   
  User.findAll(
    {
      // where:{user_role:"customer"},
      order: [["updatedAt" ,'DESC']],
    }
  ).then(userresult => {

         res.status(200).json({
           usersList: userresult
       });
     }).catch(err => {
      return res.status(500).json({
        message: "User Update failed",
        error: err.message || "internal server error!"
      });
    });
}

exports.getAllBookings = async (req, res, next) => {
  // User.findOne({ where: { id: req.query.id },
  //   order: [["createdAt" ,'DESC']],
  //     include: [
  //       { association: 'Passengers', 
  //           include:[
  //               { association: 'Bookings',distinct:true,
  //               required : true,
  //                   include:[{
  //                     association: 'Flight'
  //                   }]
  //                 },
  //           ]
  //       },
  //     ] 
  //   }).then(reponse=>{
  //   return res.status(200).json({
  //     bookinglist:reponse,
  //   });
  // }).catch(err=>{
  //   return res.status(500).json({
  //     message: "User Update failed",
  //     error: err.message || "internal server error!"
  //   });
  // })
  booking.findAll({where: { userId: req.query.id },
    attributes: [
        [sequelize.fn('DISTINCT', sequelize.col('orderId')) ,'orderId'],
    ] 
}).then(async ( result) => {

  if(result.length!=0)
  {
    let arr=[];
    for(var i=0;i<result.length;i++)
    {
        await booking.findOne({where:{orderId:result[i].dataValues.orderId},
            include: [
                { association: 'Flight' },
                { association: 'Passenger',
                    include:[
                        { association: 'User' },
                    ]
                },
        ]} ).then(async response=>{
            arr.push(response);
            if(i+1==result.length)
            {
                console.log('4');

                res.status(200).json({
                    bookingsList: arr
                });        
            }
        })
    }
  }
  else
  {
    res.status(200).json({
      message:'No Booking found.'
    });        
  }
}).catch((err) => {
    return res.status(500).json({
        message: "Bookings Request failed.",
        error: err.message || "internal server error!"
      });
});
}