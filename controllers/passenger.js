dataBase = require('../models');
const Passenger = dataBase.Passenger;
const Booking = dataBase.Booking;
const { nanoid } = require('nanoid');

exports.createPassenger = async (req, res, next) => {
  var orderId = nanoid(10);
  for(var i=0;i<req.body.total_legth;i++)
    {
      if(req.body.allProfiles[i].userId)
      {
        const userObject = {
              first_name: req.body.allProfiles[i].firstName,
              last_name: req.body.allProfiles[i].lastName,
              email: req.body.allProfiles[i].email,
              contact_number: req.body.allProfiles[i].mobile,
              address: req.body.allProfiles[i].address,
              apartment:req.body.allProfiles[i].apartment,
              city:req.body.allProfiles[i].city,
              country:req.body.allProfiles[i].country,
              postal_code:req.body.allProfiles[i].postal_code,
              nic_number:req.body.allProfiles[i].nic_number,
              passport_number:req.body.allProfiles[i].passport_number,
              userId:req.body.allProfiles[i].userId,
              user_email:req.body.allProfiles[i].useremail,
              payment_slip:req.body.allProfiles[i].url,
              payment_verified:false
          };    
          await Passenger.create(userObject)
            .then(async userResponse => {
                await Booking.create({passengerId:userResponse.dataValues.id,flightId:req.body.allProfiles[0].flightId,orderId:orderId,userId:req.body.allProfiles[i].userId}).then(
                  async bookingresponse=>{
                    if(i+1==req.body.total_legth)
                    {
                      return res.status(200).json({
                        message: "Booking Created  Successful.",
                        user: bookingresponse,
                      })                            
                    }
                }).catch(err=>{
                  return res.status(400).json({
                    message: "Booking Creation failed!",
                    error: err.message || "Internal server Error"
                  })  
                })
    
          })
          .catch(err => {
              return res.status(400).json({
                  message: "Company Creation failed!",
                  error: err.message || "Internal server Error"
              })
        }); 
    
      }
      else
      {
        return res.status(400).json({
          message: "Sign-in to create booking.",
          error: err.message || "Internal server Error"
        })
      }
    }
  
}