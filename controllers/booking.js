dataBase = require('../models');
const booking = dataBase.Booking;
const sequelize = require("sequelize");
exports.getAllBookings = async (req, res, next) => {
    booking.findAll({
        attributes: [
            [sequelize.fn('DISTINCT', sequelize.col('orderId')) ,'orderId'],
        ] 
    }).then(async ( result) => {
        let arr=[];
        for(var i=0;i<result.length;i++)
        {
            await booking.findOne({where:{orderId:result[i].dataValues.orderId},
                include: [
                    { association: 'Flight' },
                    { association: 'Passenger',
                        include:[
                            { association: 'User' },
                        ]
                    },
            ]} ).then(async response=>{
                arr.push(response);
                if(i+1==result.length)
                {
                    console.log('4');

                    res.status(200).json({
                        bookingsList: arr
                    });        
                }
            })
        }
    }).catch((err) => {
        return res.status(500).json({
            message: "Bookings Request failed.",
            error: err.message || "internal server error!"
          });
    });
}
exports.getBookingByOrderId = async (req, res, next) => {
    booking.findAll({where:{orderId:req.query.orderId},
        order: [["updatedAt" ,'DESC']],
        include: [
            { association: 'Flight' },
                { association: 'Passenger',
                include:[
            { association: 'User' },
                ]},
          ] 
    }).then((result) => {
        res.status(200).json({
            bookingsList: result
        });
    }).catch((err) => {
        return res.status(500).json({
            message: "Bookings Request failed.",
            error: err.message || "internal server error!"
          });
    });
}

exports.getBookingById = async (req, res, next) => {
    booking.findByPk(req.query.id,{
        order: [["updatedAt" ,'DESC']],
        include: [
            { association: 'Flight' },
                { association: 'Passenger',
                include:[
            { association: 'User' },
                ]},
          ] 
    }).then((result) => {
        res.status(200).json({
            bookingsList: result
        });
    }).catch((err) => {
        return res.status(500).json({
            message: "Bookings Request failed.",
            error: err.message || "internal server error!"
          });
    });
}