'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    return Promise.all([
      queryInterface.addColumn('passenger', 'payment_slip', {
        type: Sequelize.STRING,
        allowNull: true
      }),
      queryInterface.addColumn('passenger', 'payment_verified', {
        type: Sequelize.BOOLEAN,
      })
    ])

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
