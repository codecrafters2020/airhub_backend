'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        unique: true
      },
         
     encryptedPassword:{
      allowNull: false,
      type: Sequelize.STRING
    },
    lastSignIn:{
      type: Sequelize.STRING
    },
    IP:{
      allowNull: false,
      type: Sequelize.STRING
    },
    signInCount:{
      type: Sequelize.INTEGER
    },
    mobile:{
     allowNull: false,
      type: Sequelize.STRING,
      unique: true
    },
    user_role: {
      allowNull: false,
      type: Sequelize.ENUM,
      values: ['admin', 'customer','manager'],      
    },

    verifed:{
      type: Sequelize.BOOLEAN
    },
    blackList: {
      type: Sequelize.BOOLEAN
    },
   
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE
    }


    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};