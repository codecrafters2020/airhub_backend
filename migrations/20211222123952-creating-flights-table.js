'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('flights', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      departing_time: {
        type: Sequelize.DATE
      },
      arrival_time: {
        type: Sequelize.DATE
      },
      departure_airport: {
        type: Sequelize.STRING
      },
      arrival_airport: {
        type: Sequelize.STRING
      },
      flight_code: {
        type: Sequelize.STRING,
      },
         
     cabin:{
      allowNull: false,
      type: Sequelize.STRING
    },
    fare_basis:{
      type: Sequelize.STRING
    },
    ticket_fare:{
      allowNull: false,
      type: Sequelize.INTEGER
    },
    tax:{
      type: Sequelize.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE
    }


    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('flights');
  }
};
