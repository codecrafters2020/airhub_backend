'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([      
      queryInterface.addColumn('booking', 'orderId', {
        type: Sequelize.STRING,
        allowNull: true
      }),
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
