const express = require("express");
const bodyParser = require("body-parser");
const { Sequelize } = require('sequelize');
//routes 
//app declaration
const app = express();
var cron = require('node-cron');
const userRoutes = require("./routes/user");
const passengerRoutes = require("./routes/passenger");
const flightRoutes = require("./routes/flight");
const contactUsRoutes = require("./routes/contactus");
const bookingRoutes = require("./routes/booking");

var cors = require('cors');


  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use("/api/user", userRoutes);
  app.use("/api/passenger", passengerRoutes);
  app.use("/api/flight", flightRoutes);
  app.use("/api/contactus", contactUsRoutes);
  app.use("/api/booking", bookingRoutes);

module.exports = app;

cron.schedule('0 0 */12 * * *', function(){

  console.log("Cron Running")
  helper.expired_docs();
  // helper.invoice_reminder();
  // helper.partial_customers();

});
