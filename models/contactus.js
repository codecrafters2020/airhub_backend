'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class contactus extends Model {
 
    static associate(models) {
      // define association here
    }
  };
  contactus.init({
    name: DataTypes.STRING,
    message: DataTypes.STRING,
    email: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'contactus',
  });
  return contactus;
};