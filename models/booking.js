'use strict';
var Sequelize = require("sequelize");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Booking extends Model {
    
    static associate(models) {
      Booking.belongsTo(models.Passenger, {foreignKey: 'passengerId'})
      Booking.belongsTo(models.Flight, {foreignKey: 'flightId'})
      Booking.belongsTo(models.User, {foreignKey: 'userId'})

    }
  };
  Booking.init({
    passengerId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    flightId: DataTypes.INTEGER,   
    orderId: DataTypes.STRING,   
  }, {
    sequelize,
    modelName: 'Booking',
    tableName: 'booking'
  });
  return Booking;
};