'use strict';
var Sequelize = require("sequelize");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Passenger extends Model {
    
    static associate(models) {
      Passenger.hasMany(models.Booking, {foreignKey:'passengerId'})
      Passenger.belongsTo(models.User, {foreignKey:'userId'})
    }
  };
  Passenger.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    contact_number: DataTypes.STRING,
    email: DataTypes.STRING,
    address: DataTypes.STRING,
    apartment: DataTypes.STRING,
    city: DataTypes.STRING,
    country: DataTypes.STRING,   
    postal_code: DataTypes.STRING,   
    nic_number: DataTypes.STRING,   
    passport_number: DataTypes.STRING,   
    userId: DataTypes.INTEGER,
    user_email: DataTypes.STRING,
    payment_slip:DataTypes.STRING,
    payment_verified:DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Passenger',
    tableName: 'passenger'
  });
  return Passenger;
};