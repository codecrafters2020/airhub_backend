'use strict';
var Sequelize = require("sequelize");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    
    static associate(models) {
    //  User.belongsTo(models.UserPrivilege, {foreignKey: 'userPrivilegeId'})
    //  User.hasMany(models.forum, {foreignKey:'createdby'})
    //  User.hasMany(models.forum_comments, {foreignKey:'createdby'})
    User.hasMany(models.Passenger, {foreignKey:'userId'})
    User.hasMany(models.Booking, {foreignKey:'userId'})

     
      // User.belongsTo(Company);
      // define association here
    }
  };
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    encryptedPassword: DataTypes.STRING,
    lastSignIn: DataTypes.STRING,
    IP: DataTypes.STRING,
    signInCount: DataTypes.INTEGER,
    mobile: DataTypes.STRING,
    user_role: {
      type: Sequelize.ENUM,
      values: ['admin', 'customer','manager']
    },
    verifed: DataTypes.BOOLEAN,
    blackList: DataTypes.BOOLEAN,
   
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users'
  });
  return User;
};