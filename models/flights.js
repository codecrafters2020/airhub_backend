'use strict';
var Sequelize = require("sequelize");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Flight extends Model {
    
    static associate(models) {
      Flight.hasMany(models.Booking, {foreignKey:'flightId'})
      
    }
  };
  Flight.init({
    departing_time: DataTypes.DATE,
    arrival_time: DataTypes.DATE,
    departure_airport: DataTypes.STRING,
    arrival_airport: DataTypes.STRING,
    flight_code: DataTypes.STRING,
    cabin: DataTypes.STRING,
    fare_basis: DataTypes.STRING,
    ticket_fare: DataTypes.INTEGER,
    tax: DataTypes.INTEGER,   
  }, {
    sequelize,
    modelName: 'Flight',
    tableName: 'flights'
  });
  return Flight;
};