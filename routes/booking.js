const express = require("express");
const router = express.Router();

const BookingController = require("../controllers/booking");
const checkAuth = require("../middlewares/check-auth");

router.get("/allbooking", BookingController.getAllBookings);
router.get("/get_booking_by_id", BookingController.getBookingByOrderId);

module.exports = router;
