const express = require("express");
const router = express.Router();

const contactUsController = require("../controllers/contactus");
const checkAuth = require("../middlewares/check-auth");

router.post("/create_contact_us", contactUsController.createContactUs);
router.get("/getAll", contactUsController.getAllContactUs);
module.exports = router;