const express = require("express");
const router = express.Router();

const PassengerController = require("../controllers/passenger");
const checkAuth = require("../middlewares/check-auth");

router.post("/create_passenger", PassengerController.createPassenger);

module.exports = router;