const express = require("express");
const router = express.Router();

const FlightController = require("../controllers/flight");
const checkAuth = require("../middlewares/check-auth");
router.get("/get_all_flight", FlightController.getAllFlight);
router.get("/get_flight_by_id", FlightController.getFlightByID);

module.exports = router;