const express = require("express");
const router = express.Router();

const UserController = require("../controllers/user");
const checkAuth = require("../middlewares/check-auth");

router.post("/signup", UserController.createUser);
router.post("/login", UserController.userLogin);

router.get("/getAllBooking", UserController.getAllBookings);

router.get("/verifyuser", UserController.verifyuser);
router.get("/unverifyuser", UserController.unverifyuser);
router.get("/allusers", UserController.getAllUsers);


router.get("/:id", UserController.getUserById);
router.put("/:id", UserController.updateUserById);
module.exports = router;
